package in.onenzeros.realfeeltemperature;

public class UnitConverter {
    public static double toCelsius(double fahrenheit) {
        return ((5.0 * (fahrenheit - 32.0)) / 9.0);
    }

    public static double toFahrenheit(double celsius) {
        return (celsius * (9.0 / 5.0) + 32.0);
    }

    public static double kmphToMps(double kmph) {
        return (0.277778 * kmph);
    }

    public static double mpsToKmph(double mps) {
        return (mps / 0.277778);
    }

}
