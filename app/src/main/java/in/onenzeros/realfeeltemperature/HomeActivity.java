package in.onenzeros.realfeeltemperature;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private AppCompatTextView tvResult;
    private AppCompatEditText etWindSpeed;
    private AppCompatEditText etHumidity;
    private AppCompatEditText etTemperature;
    private AppCompatButton btnCalculate;
    private Spinner spinnerTemp;
    private Spinner spinnerWind;
    private Spinner spinnerHumidity;
    private ArrayList<String> tempUnits = new ArrayList<String>();
    private ArrayList<String> windSpeedUnits = new ArrayList<String>();
    private ArrayList<String> humidityUnit = new ArrayList<String>();
    private ArrayAdapter<String> spinnerTempAdapter;
    private ArrayAdapter<String> spinnerWindAdapter;
    private ArrayAdapter<String> spinnerHumidityAdapter;
    double windSpeed;
    double humidity;
    private double temperature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initComponents();
    }

    private void initComponents() {

        spinnerTemp = findViewById(R.id.spinnerTemp);
        spinnerWind = findViewById(R.id.spinnerWind);
        spinnerHumidity = findViewById(R.id.spinnerHumidity);
        tempUnits.add((char) 0x00B0 + "C");
        tempUnits.add((char) 0x00B0 + "F");
        windSpeedUnits.add("m/s");
        windSpeedUnits.add("kmph");
        humidityUnit.add("%");
        spinnerTemp.setAdapter(new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, tempUnits));
        spinnerWind.setAdapter(new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, windSpeedUnits));
        spinnerHumidity.setAdapter(new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, humidityUnit));
        spinnerTemp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        try {
                            etTemperature.setText(String.format("%.2f", UnitConverter.toCelsius(Double.parseDouble(etTemperature.getText().toString()))));
                        } catch (NumberFormatException e) {

                        } catch (Exception e) {

                        }
                        break;
                    case 1:
                        try {
                            etTemperature.setText(String.format("%.2f", UnitConverter.toFahrenheit(Double.parseDouble(etTemperature.getText().toString()))));
                        } catch (NumberFormatException e) {

                        } catch (Exception e) {

                        }
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerWind.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        try {
                            etWindSpeed.setText(String.format("%.2f", UnitConverter.kmphToMps(Double.parseDouble(etWindSpeed.getText().toString()))));
                        } catch (NumberFormatException e) {

                        } catch (Exception e) {

                        }
                        break;
                    case 1:
                        try {
                            etWindSpeed.setText(String.format("%.2f", UnitConverter.mpsToKmph(Double.parseDouble(etWindSpeed.getText().toString()))));
                        } catch (NumberFormatException e) {

                        } catch (Exception e) {

                        }
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        etWindSpeed = findViewById(R.id.etWindSpeed);
        etHumidity = findViewById(R.id.etHumidity);
        etTemperature = findViewById(R.id.etTemperature);
        tvResult = (AppCompatTextView) findViewById(R.id.tvResult);
        btnCalculate = findViewById(R.id.btnCalculate);
        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValid()) {
                    try {
                        windSpeed = Double.parseDouble(etWindSpeed.getText().toString());
                    } catch (NumberFormatException e) {
                        if (spinnerWind.getSelectedItem().toString().equals("kmph"))
                            windSpeed = 15.0;
                        else
                            windSpeed = 4.16;
                    } catch (Exception e) {

                    }
                    try {
                        humidity = Double.parseDouble(etHumidity.getText().toString());
                        temperature = Double.parseDouble(etTemperature.getText().toString());
                        if (spinnerTemp.getSelectedItem().toString().equals((char) 0x00B0 + "F"))
                            temperature = UnitConverter.toCelsius(temperature);

                        RealFeelTemperature realFeelTemperature = new RealFeelTemperature(humidity, windSpeed, temperature);
                        tvResult.setText(String.format("%.2f", realFeelTemperature.computeRealFeelTemperature()) + (char) 0x00B0 + "C");
                    } catch (NumberFormatException e) {

                    } catch (Exception e) {

                    }

                }
            }
        });
    }

    private boolean isValid() {
        if (etTemperature.getText().toString().equals("") || etHumidity.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Required Temperature and Humidity", Toast.LENGTH_LONG).show();
            return false;
        } else
            return true;
    }
}